﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EFCore2.Models
{
    public interface IProductRepository
    {
        // This interface, dont use public, private, procted
        IQueryable<Product> Products { get; }
    }
}
